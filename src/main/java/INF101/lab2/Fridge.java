package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridge = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridge.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        //Checks if the fridge is full
        if (nItemsInFridge() == totalSize()) {
            //No space left for more items
            return false;
        }else{
            //Item was added
            fridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {

        //Checks if current item is in the fridge
        if (fridge.contains(item)){
            //takes out item
            fridge.remove(item);
        }else{
            //Current item not in fridge, throws exception
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        //Removes all items
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> badList = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i++) {
            if (fridge.get(i).hasExpired()){
                badList.add(fridge.get(i));
            }
        }

        //Removes bad items from fridge
        for (int j = 0; j < badList.size(); j++) {
            fridge.remove(badList.get(j));
        }
        return badList;
    }

    public static void main(String[] args) {
        IFridge fridge = new Fridge();
        fridge.totalSize();
        System.out.println(fridge);
    }
}

